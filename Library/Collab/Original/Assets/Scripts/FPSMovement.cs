﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FPSMovement : MonoBehaviour
{
    CharacterController cc;
    WeaponsAnimsController wac;
    public float speed;
    public float jumpSpeed = 15.0F;
    public float gravity = 30.0F;
    private Vector3 moveDirection = Vector3.zero;
    public static int m_weaponnumber;
    public GameEvent walk;
    public GameEvent idle;
    public GameEvent healththings;

    public int vida;
    public int kills = 0;
    // Start is called before the first frame update
    void Start()
    {
        vida = 100;
        cc = this.GetComponent<CharacterController>();
        //Es el script de aniamciones que tiene el personaje para poner los boleanos en true o false. 
        wac = this.GetComponent<WeaponsAnimsController>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.LeftShift)) speed *= speed;
        else speed = speed;

        if (Input.GetKeyDown(KeyCode.H))
        {
            //Llama a la funcion show del script de animaciones que tiene para que ponga active un trigger.
            wac.Show();
        }
        float ver = Input.GetAxis("Vertical");
        float hor = Input.GetAxis("Horizontal");
        if (Input.GetAxis("Vertical") == 1)
        {
            if (this.GetComponent<WeaponSwitch>().aim)
            {           
                //Llame a funciones para poner en true las animaciones
                wac.AimWalk(true);
                wac.Walk(false);
            }
            else
            {
                wac.Walk(true);
                wac.AimWalk(false);
            }
        }
        else wac.Walk(false);
        //movimiento del char controller
        Vector3 movement = transform.forward * ver + transform.right * hor;
        cc.SimpleMove(speed * Time.deltaTime * movement);
        //el salto
        if (cc.isGrounded && Input.GetButton("Jump"))
        {
            moveDirection.y = jumpSpeed;
        }
        moveDirection.y -= gravity * Time.deltaTime;
        cc.Move(moveDirection * Time.deltaTime);
    }
    public void ImDead()
    {
        if (vida <= 0)
        {
            SceneManager.LoadScene(0);
            Cursor.lockState = CursorLockMode.None; 
            Cursor.visible = true;
        }
    }
    public void damagePlayer(int damage)
    {
        healththings.Raise();
        vida -= damage;
        ImDead();
    }

}
