﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class resetWeapons : MonoBehaviour
{
    public WeaponScriptableObject[] wso;
    // Start is called before the first frame update
    void Start()
    {
        wso[0].chargerS = 20;
        wso[0].MaxBalas = 120;
        wso[1].chargerS = 30;
        wso[1].MaxBalas = 150;
        wso[2].chargerS = 5;
        wso[2].MaxBalas = 20;
        wso[3].chargerS = 30;
        wso[3].MaxBalas = 120;
        wso[4].chargerS = 7;
        wso[4].MaxBalas = 30;
    }
}
