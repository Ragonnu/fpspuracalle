﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class EnemyAI : MonoBehaviour
{
    private Transform target;
    private NavMeshAgent navMess;
    public WeaponScriptableObject weapon;
    public GameObject blood;
    public GameObject pj;
    public GameObject spw;
    public GameEvent spawnEnemy;
    public GameEvent enemyDeath;
    public bool inChase = false;
    public bool isFar = false;
    public AudioClip shootClip;
    public float health = 100;
    private int damage;
    public GameObject[] patrolPoints;
    private AudioSource sound;
    public AudioClip horrorClip;
    int numerobalas;
    void Start()
    {
        Load();
    }

    void Update()
    {
        LookAtMe();
    }
    public void Load()
    {
        sound = GetComponent<AudioSource>();
        pj = GameObject.Find("PsPlayerr");
        target = pj.transform;
        spw = GameObject.Find("SpawnManager");
        navMess = GetComponent<NavMeshAgent>();
        Patrol();
    }

    private void randomPoint(int actualPoint)
    {
        bool go = false;
        while (!go)
        {
            int r = Random.Range(0, 4);
            if (r != actualPoint)
            {
                //print("Me dirijo a: " + r);
                go = true;
                navMess.SetDestination(patrolPoints[r].transform.position);
            }
        }
    }
    private void Patrol()
    {
        if(!inChase)
        {
            randomPoint(Random.Range(0, 4));
        }
    }

    public void imAlive(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            StartCoroutine(Muerte());
        }
    }

    IEnumerator Muerte()
    {
        pj.GetComponent<FPSMovement>().kills++;
        enemyDeath.Raise();
        this.transform.GetChild(0).gameObject.SetActive(true);
        yield return new WaitForSeconds(2f);
        numerobalas = Random.Range(2, 10);
        for (int i = 0; i < numerobalas; i++)
        {
            //print("he creado balas");
            GameObject aux = Pool.Instance.GetBalas();
            aux.GetComponent<Transform>().position = new Vector3(this.transform.position.x + Random.Range(0, 0.5f), this.transform.position.y-1f, this.transform.position.z);
            aux.gameObject.transform.localScale = new Vector3(10, 10, 10);
        }
        GameObject.Destroy(this.gameObject);
        //spw.GetComponent<SpawnManager>().EnemySpawn();
    }
    public void Alert()
    {
        float distance = Vector3.Distance(target.transform.position, this.transform.position);
        if (distance < 50)
        {
            if (!inChase)
            {
                inChase = true;
                StartCoroutine(Chase());
                
            }
        }
    }

    private void LookAtMe()
    {
        float distance = Vector3.Distance(target.transform.position, this.transform.position);
        Vector3 alFrente = -this.transform.forward;
        if (distance > 50) isFar = true;
        else isFar = false;
        if (distance < 30 && !inChase) Alert(); ;
        Quaternion rotacion = Quaternion.FromToRotation(alFrente, (pj.transform.position - this.transform.position)) * this.transform.rotation;
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, rotacion, Time.fixedDeltaTime);
        
        
    }


    IEnumerator Chase()
    {
        //print("Entro en Chase");
        while (!isFar)
        {
            //print("Entro en while");

            RaycastHit hit;
            if (Physics.Raycast(transform.GetChild(4).GetComponent<Transform>().position, (pj.transform.position - this.transform.position), out hit, 30000))
            {
                //print(hit.transform.tag);
                //Debug.DrawRay(this.transform.position, (pj.transform.position - this.transform.position));
                float distance = Vector3.Distance(target.transform.position, this.transform.position);
                //print(distance);

                if (hit.transform.tag == "pj" && distance < weapon.rangeS)
                {
                    //print("te diparo");
                    Shoot();
                    navMess.SetDestination(this.transform.position);
                }
                else navMess.SetDestination(target.position);
            }
            yield return new WaitForSeconds(1f);
        }
        navMess.SetDestination(this.transform.position);
        inChase = false;
        Patrol();
    }

    private void Shoot()
    {
        RaycastHit hit;
        AudioSource.PlayClipAtPoint(shootClip, this.transform.position);
        if (Physics.Raycast(transform.GetChild(4).GetComponent<Transform>().position, (pj.transform.position - this.transform.position), out hit, 30000))
        {
            Debug.DrawRay(this.transform.position, (pj.transform.position - this.transform.position), Color.red, 1f);
            print(hit.transform.name);
            if (hit.transform.tag == "pj")
            {
                print("Te doy de lleno");
                if(Random.Range(1, 10) > 8)
                {
                    damage = Random.Range(4, 18);
                    hit.transform.GetComponent<FPSMovement>().damagePlayer(damage);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //print(" entro en trigger: " + other.transform.tag);
        if (other.transform.tag == "point0")
        {
            if (!inChase)
            {
                randomPoint(0);
            }
        }
        else if (other.transform.tag == "point1")
        {
            if (!inChase)
            {
                randomPoint(1);
            }
        }
        else if (other.transform.tag == "point2")
        {
            if (!inChase)
            {
                randomPoint(2);
            }
        }
        else if (other.transform.tag == "point3")
        {
            if (!inChase)
            {
                randomPoint(3);
            }
        }
    }
}
