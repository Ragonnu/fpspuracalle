﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[CreateAssetMenu]
public class WeaponScriptableObject : ScriptableObject
{
    public int weaponNumber;
    public int chargerS;
    public int MaxchargerS;
    public int MaxBalas;
    public int damage;
    public float timeBetweenShots;
    public float dispersion;
    public float rangeS;
    public float momentoS;
    public float zoomMira;
    public bool semiAuto;
    public int nShotsOnSemiAuto;
    public GameObject impactS;
    public AudioClip shootClipS;
    public AudioClip emptyShootClipS;
    public AudioClip rechargeClip;
    public Sprite foto;
    public Sprite fotoBala;
}
