﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class CsScriptableObject : ScriptableObject
{
    public float Altura;
    public float Anchura;
    public int DefMov;
    public Color color;
    public bool dinamico;
    public int ResizedMov;
    public int ResizeVel;

}
