﻿using System;
using System.Collections;
using UnityEngine;
public class FirstPersonLook : MonoBehaviour
{
    [SerializeField]
    Transform character;
    Vector2 currentMouseLook;
    Vector2 appliedMouseDelta;
    public float sensitivity = 2;
    public float smoothing = 2;
    private bool isShoting;
    Vector3 posoriginal;




    void Start()
    {
        posoriginal = transform.localPosition;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        if (!isShoting)
        {
            // Get smooth mouse look.
            Vector2 smoothMouseDelta = Vector2.Scale(new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y")), Vector2.one * sensitivity * smoothing);
            appliedMouseDelta = Vector2.Lerp(appliedMouseDelta, smoothMouseDelta, 1 / smoothing);
            currentMouseLook += appliedMouseDelta;
            currentMouseLook.y = Mathf.Clamp(currentMouseLook.y, -90, 90);

            // Rotate camera and controller.
            transform.localRotation = Quaternion.AngleAxis(-currentMouseLook.y, Vector3.right);
            character.localRotation = Quaternion.AngleAxis(currentMouseLook.x, Vector3.up);
        }
        else
        {
            //Habiamos puesto un recoil para que la camara vibrase un poco pero hace shiquito al personaje. Que en paz descanse,
            //StartCoroutine(RecoilCamara(0.2f));
            isShoting = false;
        }
    }

    IEnumerator RecoilCamara(float duracion)
    {
        float elapsed = 0.0f;
        while (elapsed < duracion)
        {
            float x = UnityEngine.Random.Range(-0.5f, 0.5f) * 0.1f;
            float y = UnityEngine.Random.Range(-0.5f, 0.5f) * 0.1f;
            transform.localPosition = new Vector3(x, y, posoriginal.z);
            elapsed += Time.deltaTime;
            yield return null;
        }
        transform.localPosition = posoriginal;
        isShoting = false; 
    }

    public void isShotin()
    {
        isShoting = true;
       /* Vector3 rotation = transform.eulerAngles;
        rotation.y = 0;
        rotation.z = 0;
        rotation.x += Mathf.LerpAngle(transform.localRotation.x, transform.localRotation.x - 5, Time.deltaTime * 10f);
        new WaitForSeconds(Mathf.LerpAngle(transform.localRotation.x, transform.localRotation.x - 5, Time.deltaTime * 10f));
        transform.eulerAngles = rotation;
        isShoting = false;*/
    
    }
   
}
