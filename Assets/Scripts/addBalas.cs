﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class addBalas : MonoBehaviour
{
    public GameEvent ge;
    private void Start()
    {
        StartCoroutine(Muerte());
    }

    IEnumerator Muerte()
    {
        yield return new WaitForSeconds(10f);
        Pool.Instance.ReturnBalas(this.gameObject);
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.name.Equals("PsPlayerr"))
        {
            collision.gameObject.GetComponent<WeaponsShotController>().GetFirstActiveChild().GetComponent<GetWeapon>().getWSO().MaxBalas++;
            Pool.Instance.ReturnBalas(this.gameObject);
            ge.Raise();
        }
    }
}
