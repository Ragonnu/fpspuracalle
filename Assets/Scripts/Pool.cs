﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    //Una pool de balas que luego va ir instanciando.
    public static Pool Instance;
    public List<GameObject> ObjetosPooleables;

    public GameObject ObjetosPools;
    public int CuantosObjetosEnLaPool;
    //Singleton
    private void Awake()
    {
        Instance = this;
    }
    //Mete los objetos en la pool
    private void Start()
    {
        ObjetosPooleables = new List<GameObject>();
        for (int i = 0; i < CuantosObjetosEnLaPool; i++)
        {
            GameObject obj = (GameObject)Instantiate(ObjetosPools);
            obj.SetActive(false);
            ObjetosPooleables.Add(obj);
        }
    }
    //Saca un enemigo de la pool.
    public GameObject GetBalas()
    {
        for (int i = 0; i < ObjetosPooleables.Count; i++)
        {
            if (!ObjetosPooleables[i].activeInHierarchy)
            {
                ObjetosPooleables[i].SetActive(true);
                return ObjetosPooleables[i];
            }
        }
        return null;
    }
    //Develve un enemigo a la pull
    public void ReturnBalas(GameObject go)
    {
        ObjetosPooleables.Add(go);
        go.SetActive(false);
    }

}
