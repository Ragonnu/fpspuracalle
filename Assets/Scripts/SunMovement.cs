﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunMovement : MonoBehaviour
{
    void Update()
    {
        //RotateAround(punto sobre el que rotar, hacia que direccion, y aqui pone angulo pero yo lo uso para la velocidad de rotacion)
        this.transform.RotateAround(new Vector3(10, -0.8f, -12), this.transform.right, 0.5f  * Time.deltaTime);
    }
}
