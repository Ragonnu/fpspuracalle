﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PickUpWeapon : MonoBehaviour
{
    public Camera camara;
    public static int arma;
    private int numeroarmas;
    public void Start()
    {

    }
    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.E))
        {
            if (numeroarmas < 3)
            {
                //Crea un raycast y comprueba si colisiona con un arma con el tag weapon. Si colisiona con un arma lo pone de hijo del personaje y 
                //le guarda el numero de su scriptable object en el playerperfs para que lo pueda cargar en la otra esena.
                RaycastHit hit;
                if (Physics.Raycast(camara.transform.position, camara.transform.forward, out hit, 500))
                {
                    Debug.DrawLine(camara.transform.position, hit.point, Color.red, 5f);
                    if (hit.transform.gameObject.tag == "weapon")
                    {
                        hit.transform.gameObject.SetActive(false);
                        hit.transform.SetParent(this.transform.GetChild(0).transform, false);
                        hit.transform.localPosition = this.transform.GetChild(0).gameObject.transform.GetChild(0).transform.localPosition;
                        hit.transform.localEulerAngles = this.transform.GetChild(0).gameObject.transform.GetChild(0).transform.localEulerAngles;
                        hit.transform.localScale = Vector3.one;
                        numeroarmas++;
                        PlayerPrefs.SetInt("ArmaNumero" + numeroarmas, hit.transform.gameObject.GetComponent<GetWeapon>().getWSO().weaponNumber);
                        print(PlayerPrefs.GetInt("ArmaNumero" + numeroarmas));
                        Destroy(hit.transform.GetComponent<CapsuleCollider>());
                    }
                }
            }// si ha llegado a 3 armas carga la otra escena.
            else SceneManager.LoadScene("SampleScene");
        }
        //Si le da a la T puede ir cambiar de arma
        if (Input.GetKeyDown(KeyCode.C))
        {
            if (FPSMovement.m_weaponnumber + 1 >= this.gameObject.transform.GetChild(0).childCount)
            {
                this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(FPSMovement.m_weaponnumber).gameObject.SetActive(false);
                this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.SetActive(true);
                FPSMovement.m_weaponnumber = 0;
            }
            else
            {
                this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(FPSMovement.m_weaponnumber).gameObject.SetActive(false);
                FPSMovement.m_weaponnumber++;
                this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(FPSMovement.m_weaponnumber).gameObject.SetActive(true);
            }
        }
        //if (Input.GetKeyDown(KeyCode.Return)){
        //    SceneManager.LoadScene("SampleScene");
        //}
    }
}
