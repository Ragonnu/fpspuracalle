﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlyMovement : MonoBehaviour
{
    CharacterController cc;
    //WeaponsAnimsController wac;
    public float speed;
    public float jumpSpeed = 15.0F;
    public float gravity = 30.0F;
    private Vector3 moveDirection = Vector3.zero;
    //public static int m_weaponnumber;
    //public GameEvent walk;
    //public GameEvent idle;

    // Start is called before the first frame update
    void Start()
    {
        cc = this.GetComponent<CharacterController>();
        //wac = this.GetComponent<WeaponsAnimsController>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //if (Input.GetKeyDown(KeyCode.H))
        //{
        //    wac.Show();
        //}

        //if (Input.GetAxis("Vertical") == 1)
        //{
        //    if (this.GetComponent<WeaponSwitch>().aim)
        //    {
        //        wac.AimWalk(true);
        //        wac.Walk(false);
        //    }
        //    else
        //    {
        //        wac.Walk(true);
        //        wac.AimWalk(false);
        //    }
        //}
        //else wac.Walk(false);
        float ver = Input.GetAxis("Vertical");
        float hor = Input.GetAxis("Horizontal");
        Vector3 movement = transform.forward * ver + transform.right * hor;
        cc.SimpleMove(speed * Time.deltaTime * movement);

        //if (cc.isGrounded && Input.GetButton("Jump"))
        //{
        //    moveDirection.y = jumpSpeed;
        //}
        moveDirection.y -= gravity * Time.deltaTime;
        cc.Move(moveDirection * Time.deltaTime);
    }
}
