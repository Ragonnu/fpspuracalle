﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{

    private Vector3 pos;
    public bool gameOver = false;
    public GameObject[] gameObjectEnemys;

    void Start()
    {
        StartCoroutine(EnemySpawn());
    }

    IEnumerator EnemySpawn()
    {
        yield return new WaitForSeconds(2);
        while (!gameOver)
        {
            int a = UnityEngine.Random.Range(0, gameObjectEnemys.Length);
            GameObject Enemy = (GameObject)Instantiate(gameObjectEnemys[a]);
            if (Enemy != null)
            {
                Enemy.SetActive(true);
                Enemy.gameObject.transform.localPosition = new Vector3(-3, -5, -90);
            }
            yield return new WaitForSeconds(10);
        }
    }

}
