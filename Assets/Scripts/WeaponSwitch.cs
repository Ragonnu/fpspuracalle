﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponSwitch : MonoBehaviour
{
    public float sensitivity = 1;

    public Camera camara;
    public float defaultzoom;
    public float zoom;
    public float defaultFOV;
    public bool aim = false;
    public GameEvent WeaponSwap;
    public GameEvent WeaponSemiAuto;
    public Image Mira;
    public GameObject crosshair;
    public GameObject sniper;
    private void Start()
    {
        Mira.gameObject.SetActive(false);
        defaultFOV = camara.fieldOfView;

    }
     void Update()
    {
        MirillaSniper();
    }
    // Cuando le da con el click del raton para hacer zoom le pone la mirilla a la sniper y le hace un zoom
    void MirillaSniper()
    {
         
        if (this.GetComponent<WeaponsShotController>().GetFirstActiveChild().GetComponent<GetWeapon>().getWSO().weaponNumber == 3){
        if (Input.GetMouseButtonDown(1))
        {
            aim = !aim;
            Mira.gameObject.SetActive(aim);
            crosshair.SetActive(!aim);

            if (aim)
            {
                    camara.fieldOfView = 7.5f;
            }
            else
            {
                camara.fieldOfView = defaultFOV;

            }
           
        }
        }
    }
    //con el scroll cambia de arma
    void FixedUpdate()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f && !Input.GetMouseButton(1)) 
        {
            ChangeWeapon();
        }
     
        if (Input.GetMouseButtonDown(2))
        {
            print("Le ha dao al de medio");
            SemiAuto();
        }
    }

    private void SemiAuto()
    {
        if (!this.GetComponent<WeaponsShotController>().GetFirstActiveChild().GetComponent<GetWeapon>().getWSO().semiAuto)
        {
            print("SEmiaut");
            this.GetComponent<WeaponsShotController>().GetFirstActiveChild().GetComponent<GetWeapon>().getWSO().semiAuto = true;            
            WeaponSemiAuto.Raise();
        }else if(this.GetComponent<WeaponsShotController>().GetFirstActiveChild().GetComponent<GetWeapon>().getWSO().semiAuto)
        {
            this.GetComponent<WeaponsShotController>().GetFirstActiveChild().GetComponent<GetWeapon>().getWSO().semiAuto = false;
            WeaponSemiAuto.Raise();
        }
    }


    private void ChangeWeapon()
    {
        if(FPSMovement.m_weaponnumber+1>= this.gameObject.transform.GetChild(0).childCount)
        {
            this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(FPSMovement.m_weaponnumber).gameObject.SetActive(false);
            this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.SetActive(true);
            FPSMovement.m_weaponnumber = 0;
        }
        else
        {
            this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(FPSMovement.m_weaponnumber).gameObject.SetActive(false);
            FPSMovement.m_weaponnumber++;
            this.gameObject.transform.GetChild(0).gameObject.transform.GetChild(FPSMovement.m_weaponnumber).gameObject.SetActive(true);
        }
        WeaponSwap.Raise();
        this.GetComponent<WeaponsShotController>().LoadScrObj();
    }
}
