﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class WeaponsShotController : MonoBehaviour
{
    public Camera camara;
    public GameObject allWeapons;
    private GameObject impacto;
    private AudioClip shootClip;
    private AudioClip emptyShootClip;
    private AudioClip rechargeClip;
    public float timeBetweenShots;
    private WeaponsAnimsController wac;
    private int actualweapon;
    public GameEvent shoting;
    public GameEvent onrecharge;
    public WeaponScriptableObject WSO;


    private bool recoil = false;
    private float recoilSpeed;
    // Start is called before the first frame update

    //Al empezar carga las armas que ha cogido en la otra escena. Tenemos un objeto llamado allWeapons donde estan las armas desactivas y esto las coge.
    public void Awake()
    {
        allWeapons = GameObject.Find("AllWeapons");
        CargarArmasScene();
        this.transform.GetChild(0).gameObject.transform.GetChild(0).transform.gameObject.SetActive(true);
    }

    void Start()
    {
        recoilSpeed = 0.01f;
        WSO = this.transform.GetChild(0).gameObject.transform.GetChild(0).GetComponent<GetWeapon>().getWSO();
        WSO = this.transform.GetChild(0).gameObject.transform.GetChild(0).GetComponent<GetWeapon>().getWSO();
        wac = this.GetComponent<WeaponsAnimsController>();
        actualweapon = FPSMovement.m_weaponnumber;
        LoadScrObj();
    }
    //Carga todas las armas que habia cogido en la otra escena.
    private void CargarArmasScene()
    {
        int a = PlayerPrefs.GetInt("ArmaNumero1");
        int b = PlayerPrefs.GetInt("ArmaNumero2");
        int c = PlayerPrefs.GetInt("ArmaNumero3");
        GameObject Arma1 = null;
        GameObject Arma2 = null;
        GameObject Arma3 = null;

        int children = allWeapons.gameObject.transform.childCount;
        for (int i = 0; i < children; ++i)
        {
            print("For loop: " + allWeapons.transform.GetChild(i).gameObject.GetComponent<GetWeapon>().getWSO().weaponNumber);
            if (allWeapons.transform.GetChild(i).gameObject.GetComponent<GetWeapon>().getWSO().weaponNumber == a)
            {
                Arma1 = allWeapons.transform.GetChild(i).gameObject;
            }else if(allWeapons.transform.GetChild(i).gameObject.GetComponent<GetWeapon>().getWSO().weaponNumber == b)
            {
                Arma2 = allWeapons.transform.GetChild(i).gameObject;
            }else if(allWeapons.transform.GetChild(i).gameObject.GetComponent<GetWeapon>().getWSO().weaponNumber == c)
            {
                Arma3 = allWeapons.transform.GetChild(i).gameObject;
            }
        }
        ponerArmaHijo(Arma1);
        ponerArmaHijo(Arma2);
        ponerArmaHijo(Arma3);

    }
    //Ponemos las armas al hijo que es la camara.
    public void ponerArmaHijo(GameObject go)
    {
        go.transform.SetParent(this.transform.GetChild(0).transform, false);
        go.transform.gameObject.SetActive(false);
        go.transform.parent = this.transform.GetChild(0).gameObject.transform;
        go.transform.localPosition = new Vector3(this.transform.GetChild(0).gameObject.transform.localPosition.x, this.transform.GetChild(0).gameObject.transform.localPosition.y-2.3f, this.transform.GetChild(0).gameObject.transform.localPosition.z+0.3f);
        go.transform.localEulerAngles = this.transform.GetChild(0).gameObject.transform.localEulerAngles;
        go.transform.localScale = Vector3.one;
    }
    //Cargamos cosas del scriptableobject.
    public void LoadScrObj()
    {
        timeBetweenShots = WSO.timeBetweenShots;
        shootClip = WSO.shootClipS;
        emptyShootClip = WSO.emptyShootClipS;
        rechargeClip = WSO.rechargeClip;
    }
    
    // Update is called once per frame
    void FixedUpdate()
    {
        Recharge();
        if (wac.CheckIfIsPlaying("Recharge"))
        {
            print("esta recargando " + wac.CheckIfIsPlaying("Recharge"));
        }
        else
        {
            Shot();
        }
        WSO = this.GetFirstActiveChild().GetComponent<GetWeapon>().getWSO();

        if (recoil)
        {
            addRecoil();
            recoil = false;
        }
    }

    private void addRecoil()
    {
        print("recoil");
        
    }

    private void Shot()
    {
        actualweapon = FPSMovement.m_weaponnumber;

    //Mira si le ha dado con el boton del raton y si el tiempo entre el disparo anterior y este ha pasado ya un tiempo.
    if (Input.GetMouseButton(0) && timeBetweenShots<=0)
    {
        LoadScrObj();
        if (WSO.chargerS > 0)
        {
                timeBetweenShots = WSO.timeBetweenShots;
                if (WSO.semiAuto)
                {
                    recoil = true;
                   StartCoroutine(ShotForSemi());
                }
                else
                {
                    recoil = true;
                    ShotNow();
                }
            }
            else
            {
            AudioSource.PlayClipAtPoint(emptyShootClip, this.transform.position);
            }
        }
        timeBetweenShots -= Time.deltaTime;
    }

    //Cuando esta puesto la arma en semiauto dispara mas veces
    IEnumerator ShotForSemi()
    {
        for (int i = 0; i < WSO.nShotsOnSemiAuto; i++)
        {
            if (WSO.chargerS > 0)
            {
                ShotNowDisperson();
                yield return new WaitForSeconds(0.3f);
            }
        }
    }
    //Dispara normal pero con un poco de dispersion.
    private void ShotNowDisperson()
    {
        RaycastHit hit;
        WSO.chargerS--;
        if (this.GetComponent<WeaponSwitch>().aim)
        {
            wac.AimShot();
        }
        else
        {
            wac.Shot();
        }
        Vector3 noRecto = new Vector3(camara.transform.forward.x + Random.Range(-WSO.dispersion, +WSO.dispersion), camara.transform.forward.y + Random.Range(-WSO.dispersion, +WSO.dispersion), camara.transform.forward.z + Random.Range(-WSO.dispersion, +WSO.dispersion));

        AudioSource.PlayClipAtPoint(WSO.shootClipS, this.transform.position);
        if (Physics.Raycast(camara.transform.position, noRecto, out hit, WSO.rangeS))
        {
            //print(hit.transform.tag);
            float percent = WSO.rangeS / hit.distance;
            shoting.Raise();
            //print("disparo a: " + hit.point);
            Debug.DrawLine(camara.transform.position, hit.point, Color.red, 1f);
            GameObject newExplosion = Instantiate(WSO.impactS);
            newExplosion.transform.position = hit.point;
            Destroy(newExplosion, 3f);
            if (hit.collider.tag == "enemy1")
            {
                hit.transform.GetComponent<EnemyAI>().imAlive(WSO.damage);
                hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * WSO.momentoS * percent, hit.point);
            }
            else if (hit.collider.tag == "head")
            {
                print("Choolazooo!!");
                hit.transform.GetComponent<EnemyAI>().imAlive(WSO.damage * 2);
                hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * WSO.momentoS * percent, hit.point);
            }
        }
    }

    //Dispara recto.
    private void ShotNow()
    {

        RaycastHit hit;
        WSO.chargerS--;
        if (this.GetComponent<WeaponSwitch>().aim)
        {
            wac.AimShot();
        }
        else
        {
            wac.Shot();
        }
        AudioSource.PlayClipAtPoint(WSO.shootClipS, this.transform.position);
        if (Physics.Raycast(camara.transform.position, camara.transform.forward, out hit, WSO.rangeS))
        {
            //.contacts[0].thisCollider;
            //Collider myCollider = hit.
            Debug.DrawLine(camara.transform.position, hit.point, Color.red, 1f);
            shoting.Raise();
            //print(hit.transform.tag);
            GameObject newExplosion = Instantiate(WSO.impactS);
            newExplosion.transform.position = hit.point;
            Destroy(newExplosion, 3f);
            //print(hit.transform.tag);
            if (hit.collider.tag == "enemy1")
            {
                print("Le he dado");
                hit.transform.GetComponent<EnemyAI>().imAlive(WSO.damage);
                hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * WSO.momentoS, hit.point);
            }
            else if (hit.collider.tag == "head")
            {
                print("Cholazo!!");
                hit.transform.gameObject.GetComponent<EnemyAI>().imAlive(WSO.damage * 2);
               // hit.transform.GetComponent<EnemyAI>().imAlive(WSO.damage * 2);
                hit.transform.gameObject.GetComponent<Rigidbody>().AddForceAtPosition(-hit.normal * WSO.momentoS , hit.point);
            }
        }
    }

    //Recarga las balas restandole a las balas totales .
    private void Recharge()
    {
        if (Input.GetKeyDown(KeyCode.R) && WSO.chargerS != WSO.MaxchargerS)
        {
            int balasParaRecargar = WSO.MaxchargerS - WSO.chargerS;
            if (WSO.MaxBalas >= balasParaRecargar)
            {
                WSO.chargerS = WSO.MaxchargerS;
                WSO.MaxBalas -= balasParaRecargar;
                wac.Recharge();
                AudioSource.PlayClipAtPoint(WSO.rechargeClip, this.transform.position);
                onrecharge.Raise();
            }
            else
            {
                WSO.chargerS += WSO.MaxBalas;
                WSO.MaxBalas = 0;
                wac.Recharge();
                AudioSource.PlayClipAtPoint(WSO.rechargeClip, this.transform.position);
                onrecharge.Raise();
            }
        }
        else if(Input.GetKeyDown(KeyCode.T))
        {
            wac.Show();
        }
    }

    //Coge al primer hijo activo de la camara.Se usa para muchas cosas.
    public GameObject GetFirstActiveChild()
    {
        for (int i = 0; i < this.transform.GetChild(0).gameObject.transform.childCount; i++)
        {
            if (this.transform.GetChild(0).gameObject.transform.GetChild(i).gameObject.activeSelf == true)
            {
                return this.transform.GetChild(0).gameObject.transform.GetChild(i).gameObject;
            }
        }
        return null;
    }
}
