﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteScript : MonoBehaviour
{
    //dos imagenes Muted/UnMuted 
    public Sprite b1;
    public Sprite b2;

    //Boton a cambiar sprite
    public Button buttonChange;

    //valor inicial Indice 
    public int buttonValue = 0;
    void Start()
    {
        buttonChange.image.sprite = b1;
    }
    /*Funcion que se llama en el OnClick del boton de sonido 
    * del menu principal que con un switch entra en las posibles 
    * opciones y cambia el sonido y la imagen del boton
    * 
    * */
    // Update is called once per frame
    public void ChangeImage()
    {
        //aumenta el indice
        buttonValue++;

        switch (buttonValue)
        {
            case 1:
                buttonChange.image.sprite = b2;
                buttonValue = 1;
                AudioListener.volume = 0;
                break;

            case 2:
                buttonChange.image.sprite = b1;
                buttonValue = 0;
                AudioListener.volume = 0.3f;
                break;
            default:
                break;
        }

    }
}
