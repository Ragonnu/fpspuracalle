﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MenuController : MonoBehaviour
{
    public CsScriptableObject CrosshairSettings;
    
   

 

    public void Play()
    {
        SceneManager.LoadScene(1);

    }
        
  
    //Funcion para cuando le das al ok sin entrar al menu de crosshair 
    public void DefaultSettings()
    {
        if(MenuCrosshairController.altura== 0 && MenuCrosshairController.anchura==0 && MenuCrosshairController.color== new Color(0,0,0,0) && MenuCrosshairController.dinamico == false)
        {
            MenuCrosshairController.altura = CrosshairSettings.Altura;
            MenuCrosshairController.anchura = CrosshairSettings.Anchura;
            MenuCrosshairController.color = CrosshairSettings.color;
            MenuCrosshairController.dinamico = CrosshairSettings.dinamico;



        }
       

    }

   
}