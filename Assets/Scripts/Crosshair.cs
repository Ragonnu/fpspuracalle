﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[ExecuteInEditMode]
public class Crosshair : MonoBehaviour
{

    public float altura = 10f;
    public float anchura = 2f;
    public float defaultMov = 10f;
    public Color color = Color.grey;
    public bool redimension = false;
    public float resizedMov = 20f;
    public float resizeVel = 3f;
    public CsScriptableObject settings;
    
    float movimiento;

    bool resizing = false;

    private void Start()
    {
       
        setSettings();
    }

    void Awake()
    {
           //set spread
           movimiento = defaultMov;
    }

    void Update()
    {

        CargarSettings();

        //si te mueves el boolean para que se mueva el crosshair es true
        if (Input.GetMouseButton(0) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D)) 
        { resizing = true; } else { resizing = false; }

        //Redimension es una variable que sirve para que el jugador decida si quiere que se mueva el crosshair o sea completamente estatico siempre
        if (redimension)
        {
            if (resizing)
            {
                //increase spread 
                movimiento = Mathf.Lerp(movimiento, resizedMov, resizeVel * Time.deltaTime);
            }
            else
            {
                //decrease spread
                movimiento = Mathf.Lerp(movimiento, defaultMov, resizedMov * Time.deltaTime);
            }
           
            //clamp spread
            movimiento = Mathf.Clamp(movimiento, defaultMov, resizedMov);
        }
    }

    void OnGUI()
    {
        Texture2D texture = new Texture2D(1, 1);
        texture.SetPixel(0, 0, color);
        texture.wrapMode = TextureWrapMode.Repeat;
        texture.Apply();

        //Dibujo parte de arriba del crosshair 
        GUI.DrawTexture(new Rect(Screen.width / 2 - anchura / 2, (Screen.height / 2 - altura / 2) + movimiento / 2, anchura, altura), texture);

        //Dibujo parte de abajo del crosshair
        GUI.DrawTexture(new Rect(Screen.width / 2 - anchura / 2, (Screen.height / 2 - altura / 2) - movimiento / 2, anchura, altura), texture);

        //Dibujo parte de izquierda del crosshair
        GUI.DrawTexture(new Rect((Screen.width / 2 - altura / 2) + movimiento / 2, Screen.height / 2 - anchura / 2, altura, anchura), texture);

        //Dibujo parte de derecha del crosshair
        GUI.DrawTexture(new Rect((Screen.width / 2 - altura / 2) - movimiento / 2, Screen.height / 2 - anchura / 2, altura, anchura), texture);
    }

    public void SetRisizing(bool state)
    {
        resizing = state;
    }

    //Cargar los settings asignados en el menu 
    public void CargarSettings()
    {

        this.altura = settings.Altura;
        this.anchura = settings.Anchura;
        this.defaultMov = settings.DefMov;
        this.resizedMov = settings.ResizedMov;
        this.redimension = settings.dinamico;
        this.resizeVel = settings.ResizeVel;
        this.color = settings.color;

    }

    //Asignas las variables del menu a las actuales
    public void setSettings()
    {
      
            settings.Altura = MenuCrosshairController.altura;
            settings.Anchura = MenuCrosshairController.anchura;
            settings.color = MenuCrosshairController.color;
            settings.dinamico = MenuCrosshairController.dinamico;
            
    }
}
