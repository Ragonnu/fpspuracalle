﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuCrosshairController : MonoBehaviour
{

    public CsScriptableObject CrosshairSettings;

    //Variables que guardan los datos
    public static float altura;
    public static float anchura;
    public static Color color;
    public static bool dinamico;

    //Variables que recogen los datos
    public Slider height;
    public Slider width;
    public Toggle dinamToggle;
    public Dropdown colorSelector;

    //Variables Default
    float DefAlt;
    float DefAnch;
    Color DefColor;
    bool DefDin;

    //Imagen de numeros de slider
    public Text AlturaTxt;
    public Text AnchuraTxt;

    // Start is called before the first frame update
    void Start()
    {

        //Inicializamos las variables
        altura = this.CrosshairSettings.Altura;
        anchura = this.CrosshairSettings.Anchura;
        color = this.CrosshairSettings.color;
        dinamico = this.CrosshairSettings.dinamico;

        //Default Values 
        DefAlt = this.CrosshairSettings.Altura;
        DefAnch = this.CrosshairSettings.Anchura;
        DefColor = this.CrosshairSettings.color;
        DefDin = this.CrosshairSettings.dinamico;

        //Valores recogidos 
        this.height.value = altura;
        this.width.value = anchura;
        this.dinamToggle.isOn = dinamico;

    }

    // Update is called once per frame
    void Update()
    {
        AlturaTxt.text = ""+altura;
        AnchuraTxt.text = "" + anchura;


        print(altura + " Altura");
        print(anchura + " anchura");
        print(DefColor + " color");
        print(dinamico + " dinamico");


        altura = this.height.value;
        anchura = this.width.value;
        switch (this.colorSelector.value)
        {
            //gris
            case 0:
                color = Color.gray;
                break;
            //Blanco
            case 1:
                color = Color.white;
                break;
            //Rojo
            case 2:
                color = Color.red;
                break;
            //Verde
            case 3:
                color = Color.green;
                break;
            //Azul
            case 4:
                color = Color.blue;
                break;
            //Amarillo
            case 5:
                color = Color.yellow;
                break;
        }
        dinamico = this.dinamToggle.isOn;
    }

    //Funcion onClick Ok button del menu Crosshair
    public void OkCrosshair()
    {
        //Guarda los datos 
        altura = this.height.value;
        anchura = this.width.value;
        switch (this.colorSelector.value)
        {
            //gris
            case 0:
                color = Color.gray;
                break;
            //Blanco
            case 1:
                color = Color.white;
                break;
            //Rojo
            case 2:
                color = Color.red;
                break;
            //Verde
            case 3:
                color = Color.green;
                break;
            //Azul
            case 4:
                color = Color.blue;
                break;
            //Amarillo
            case 5:
                color = Color.yellow;
                break;
        }
        dinamico = this.dinamToggle.isOn;



    }

    //Funcion OnClick BackButton menu Crosshair
    public void BackCrosshair()
    {
        //Deja los datos anteriores 
        altura = DefAlt;
        anchura = DefAnch;
        color = DefColor;
        dinamico = DefDin;

    }
}
