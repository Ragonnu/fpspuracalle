﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraDeVida : MonoBehaviour
{
    private Image Barra;
    public Text numero;
    private float CurrentHP;
    public float MaxHP = 100f;

    //Donde este la vida
    FPSMovement player;

    // Start is called before the first frame update
    void Start()
    {
        Barra = GetComponent<Image>();
        player = FindObjectOfType<FPSMovement>();
    }

    /**Igualas currentHp a la del personaje
    * y lo divides entre la maxima para del
    * 0 al 1 mover el fillAmount de la imagen
    */
    private void Update()
    {
        numero.text = "" + player.vida;
        CurrentHP = player.vida;
        Barra.fillAmount = CurrentHP / MaxHP;
    }
    public void LifeThings()
    {
        numero.text = "" + player.vida;
        CurrentHP = player.vida;
        Barra.fillAmount = CurrentHP / MaxHP;
    }
}
