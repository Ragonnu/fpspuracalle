﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponsAnimsController : MonoBehaviour
{
    //Este script solo sirve para poner las animaciones de cada arma.
    public AudioClip weaponSwap;
    public int actualweapon;
    private GameObject arma;
    // Start is called before the first frame update
    void Start()
    {
        actualweapon = FPSMovement.m_weaponnumber;
        actualweapon = FPSMovement.m_weaponnumber;
        arma = this.GetComponent<WeaponsShotController>().GetFirstActiveChild();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        actualweapon = FPSMovement.m_weaponnumber;
        arma = this.GetComponent<WeaponsShotController>().GetFirstActiveChild();

    }


    public void Walk(bool mvalue)
    {
        arma.GetComponent<Animator>().SetBool("walk", mvalue);
    }
    public void Run(bool mvalue)
    {
        arma.GetComponent<Animator>().SetBool("run", mvalue);
    }

    public void Recharge()
    {
        arma.GetComponent<Animator>().SetTrigger("reload");
    }

    public void Shot()
    {
        arma.GetComponent<Animator>().SetTrigger("shot");
    }
    public void Show()
    {
        arma.GetComponent<Animator>().SetTrigger("show");
    }

    public void AimShot()
    {
        arma.GetComponent<Animator>().SetTrigger("aimshot");
    }

    public void AimWalk(bool mvalue)
    {
        arma.GetComponent<Animator>().SetBool("aimwalk", mvalue);
    }

    public void AimIdle(bool mvalue)
    {
        arma.GetComponent<Animator>().SetBool("aim", mvalue);
    }
    public bool CheckIfIsPlaying(string animationname)
    {
        if (arma.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName(animationname))
        {
            return true;
        }
        return false;
    }

    public float GetDurationOfAnim()
    {
        return arma.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length;
    }
}
