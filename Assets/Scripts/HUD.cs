﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    //Imagenes y Text del HUD 
    public Image imgArma;
    public Image imgBala;
    public Text balas;
    public Text kills;
    private GameObject player;

    private void Start()
    {
        player = GameObject.Find("PsPlayerr");

        //Asignar las Image
        GameObject imageArma = GameObject.FindGameObjectWithTag("imgarma");
        if (imageArma != null)
        {
            imgArma = imageArma.GetComponent<Image>();
        }

        //Asignar las Image
        GameObject imageBala = GameObject.FindGameObjectWithTag("imgbala");
        if (imageBala != null)
        {
            imgBala = imageBala.GetComponent<Image>();
        }

        //Cargas las fotos iniciales de las armas
        imgArma.sprite = player.GetComponent<WeaponsShotController>().GetFirstActiveChild().GetComponent<GetWeapon>().getWSO().foto;
        imgBala.sprite = player.GetComponent<WeaponsShotController>().GetFirstActiveChild().GetComponent<GetWeapon>().getWSO().fotoBala;
        //Cargas el marcador de balas inicial
        balas.text = player.GetComponent<WeaponsShotController>().GetFirstActiveChild().GetComponent<GetWeapon>().getWSO().chargerS + "/" + player.GetComponent<WeaponsShotController>().GetFirstActiveChild().GetComponent<GetWeapon>().getWSO().MaxchargerS + "/" + player.GetComponent<WeaponsShotController>().GetFirstActiveChild().GetComponent<GetWeapon>().getWSO().MaxBalas;
        //Cargas el contador de Kills inicial
        kills.text = "Kills " + player.GetComponent<FPSMovement>().kills;
       

    }


    public void OnEventRise()
    {
        balas.text = player.GetComponent<WeaponsShotController>().GetFirstActiveChild().GetComponent<GetWeapon>().getWSO().chargerS + "/" + player.GetComponent<WeaponsShotController>().GetFirstActiveChild().GetComponent<GetWeapon>().getWSO().MaxchargerS + "/" + player.GetComponent<WeaponsShotController>().GetFirstActiveChild().GetComponent<GetWeapon>().getWSO().MaxBalas;
        imgArma.sprite = player.GetComponent<WeaponsShotController>().GetFirstActiveChild().GetComponent<GetWeapon>().getWSO().foto;
        imgBala.sprite = player.GetComponent<WeaponsShotController>().GetFirstActiveChild().GetComponent<GetWeapon>().getWSO().fotoBala;
    }

    public void killsChange()
    {
        kills.text = "Kills " + player.GetComponent<FPSMovement>().kills;
    }
}
